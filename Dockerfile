FROM python:3.8-slim

RUN apt-get update && apt-get install -y \
    postgresql-client \
    cron

RUN pip install s3cmd 

WORKDIR /usr/src/app

COPY src/backup.sh src/entrypoint.sh /usr/src/app/

ENTRYPOINT [ "sh" ]

VOLUME [ "/backups" ]

CMD ["/usr/src/app/entrypoint.sh"]
