DOCKER_LOCAL = -f docker-compose.local.yml
MAIN_CONTAINER_NAME = backup_database

local:
	# start develop environment
	docker-compose $(DOCKER_LOCAL) up ${ARGS}

local-d:
	# start develop environment in the deamon
	ARGS=-d make local

local-build:
	# build docker images of the develop environment
	docker-compose $(DOCKER_LOCAL) build ${ARGS}

local-build-clean:
	# build docker images of the develop environment with no cache
	ARGS=--no-cache make local-build

local-stop:
	# stop containers develop environment
	docker-compose $(DOCKER_LOCAL) stop
	
shell:
	# docker exec with shell in the main container
	docker exec -it $(MAIN_CONTAINER_NAME) sh


