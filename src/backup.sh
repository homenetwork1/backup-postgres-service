#!/bin/sh

echo "$(date): backup process started"

echo "$(date): pg_dump started for ${PGDATABASE}"

if [[ -z "${PGUSER_FILE}" ]]; then
    PGUSER_SECRET=${PGUSER}
else
    PGUSER_SECRET=`cat ${PGUSER_FILE}`
fi

if [[ -z "${PGPASSWORD_FILE}" ]]; then
    PGPASSWORD_SECRET=${PGPASSWORD}
else
    PGPASSWORD_SECRET=`cat ${PGPASSWORD_FILE}`
fi


if [[ -z "${PGDATABASE_FILE}" ]]; then
    PGDATABASE_SECRET=${PGDATABASE}
else
    PGDATABASE_SECRET=`cat ${PGDATABASE_FILE}`
fi

FILE=/backups/$PGDATABASE-$(date +\%FT\%H-%M-%S).sql.gz

PGPASSWORD=$PGPASSWORD_SECRET pg_dump -U $PGUSER_SECRET $PGDATABASE_SECRET | /bin/gzip > $FILE

echo "$(date): pg_dump completed"

if [ -z "$S3_HOST" ]; then :
  else
    if [ -z "$S3_BUCKET"] || [-z "$S3_ACCESS_KEY"] || [-z "$S3_SECRET_KEY" ]; then
      echo "Some required S3 env variable(s) are missing"
    else
      echo "$(date): S3 backup uploading started"
      s3cmd put $FILE s3://$S3_BUCKET --access_key=$S3_ACCESS_KEY --secret_key=$S3_SECRET_KEY --host=$S3_HOST
      echo "$(date): S3 backup uploading completed"
      rm $FILE
      echo "$(date): local backup ${FILE} removed"
    fi
fi

echo "$(date): backup process completed"
