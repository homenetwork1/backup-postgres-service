
#!/bin/sh

echo "$(date): backup service started"

echo "${CRON_SCHEDULE}    /usr/src/app/backup.sh" > cronscheduler.txt
crontab cronscheduler.txt
cron -f
