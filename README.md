# backup-postgres-service

A small service to create backups of PostgreSQL database

Required variables

    PGHOST
    PGPORT
    PGDATABASE
    PGUSER
    PGPASSWORD

> for PGUSER, PGPASSWORD and PGDATABASE we can append \_FILE to use docker secrets instead

Optional Variables

    S3_ACCESS_KEY
    S3_SECRET_KEY
    S3_HOST
    CRON_SCHEDULE="* * * * *"
